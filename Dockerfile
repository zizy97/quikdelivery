FROM openjdk:11

ENV PORT=$PORT \
    AWS_ACCESS_KEY=$AWS_ACCESS_KEY \
    AWS_SECRET_KEY=$AWS_SECRET_KEY \
    DB_PASSWORD=$DB_PASSWORD \
    DB_USERNAME=$DB_USERNAME \
    GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID \
    GOOGLE_CLIENT_SECRET=$GOOGLE_CLIENT_SECRET \
    JWT_SECRET=$JWT_SECRET \
    MAIL_PASSWORD=$MAIL_PASSWORD \
    MAIL_USERNAME=$MAIL_USERNAME

RUN mkdir -p /home/app

COPY ./target/quikapp-backend.jar /home/app/

CMD ["java", "-jar", "/home/app/target/quikapp-backend.jar"]

EXPOSE 8081
